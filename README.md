# kmDebugTest Application

This is a simple test application for an AVR microcontroller. It initializes the debugging system and toggles a debug pin at regular intervals.

## Overview

The `kmDebugTest` application demonstrates the usage of the [kmDebug]((https://gitlab.poland/avr/kmAvrLibs/kmDebug)) library for AVR microcontrollers.

## Installation

To get the `kmDebugTest` application from GitLab, use the following command:

```bash
git clone git@gitlab.com:c-mos/kmAvrTests/kmDebugTest.git
cd kmDebugTest
git submodule update --init
git submodule foreach git checkout main
```
Software has been compiled and tested with [**Atmel Studio 7**](http://atmel-studio.s3-website-us-west-2.amazonaws.com/7.0.2397/as-installer-7.0.2397-full.exe)

## Overview

The application demonstrates basic usage of the `kmDebug` library to control an output pin. The pin toggles its state every 500 milliseconds, providing a simple way to verify that the microcontroller is running and the debugging system is operational.

## Features

- **Debug Pin Initialization**: Initializes all ports with pull-up resistors and sets up the debugging system.
- **Pin Toggling**: Toggles the state of a specified debug pin every 500 milliseconds.

## Functions

### `appInit`

This function initializes the debugging system:

- Sets all ports to use pull-up resistors.
- Initializes the debugging system.
- Ensures the debug pin (`DB_PIN_0`) is turned off initially.

### `appLoop`

This function contains the main application loop:

- Toggles the state of the debug pin (`DB_PIN_0`).
- Introduces a delay of 500 milliseconds between toggles.

## Usage

### Building and Flashing

1. Clone the repository.
2. Open the project in your development environment.
3. Build the project to generate the firmware.
4. Flash the firmware onto your AVR microcontroller using your preferred flashing tool.

### Running the Application

1. Ensure the microcontroller is powered and connected correctly.
2. The debug pin (`DB_PIN_0`) will toggle its state every 500 milliseconds.

### Debugging

The application uses the `kmDebug` library for debugging purposes. Monitor the state of the debug pin to verify the application is running correctly.

## Dependencies

This application depends on the following libraries:
- [kmFrameworkAVR](https://gitlab.com/c-mos/kmAvrLibs)
  - [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)
  - [kmDebug](https://gitlab.com/c-mos/kmAvrLibs/kmDebug)

Ensure that these libraries are properly included and configured in your project.

## Author and License
Author: Krzysztof Moskwa

e-mail: chris[dot]moskva[at]gmail[dot]com

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)
